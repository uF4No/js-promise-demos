//actionsES7.js

//async function that simulates reading a file, takes 2secs.
async function readFile(filename){
  //if filename is empty rejects promise
  if(filename != ''){
    return new Promise(function(resolve) {
        setTimeout( () => {
          resolve('file OK')
        }, 2000)
    })
  }else{
    const error = new Error('File does not exist');
    return Promise.reject(error);
  }
}

//async function that simulates an API call, takes 3secs
async function callApi(fileContent){
  //if fileContent is diferent from 'file OK' rejects promise 
  if(fileContent == 'file OK'){
    return new Promise(function(resolve) {
        setTimeout( () => {
          resolve('Response from API')
        }, 3000)
    })
  }else{
    const reason = 'File is not OK';
    return Promise.reject(reason);
  }
}

//async function that prints the message
async function sendResponse(apiRes){
  console.log('Sending response: ' + apiRes);
  return;
}

//anonymous async function to start the execution
(async () => {
  try{
    console.log('before readFile()');
    let responseReadFile = await readFile('awdwad');
    console.log('after readFile(): ' + responseReadFile);
    
    console.log('before callApi()');
    let responseCallApi = await callApi(responseReadFile);
    console.log('after callApi(): ' + responseCallApi);

    sendResponse(responseCallApi);
  }catch(error){
    console.log(error)
  }
})();


