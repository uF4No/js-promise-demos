//actions.js

//simulates reading a file, takes 2secs.
function readFile(filename){
  //if filename is empty rejects promise
  if(filename != ''){
    return new Promise(function(resolve) {
        setTimeout( () => {
          resolve('file OK')
        }, 2000)
    })
  }else{
    const error = new Error('File does not exist');
    return Promise.reject(error);
  }
}

//simulates an API call, takes 3secs
function callApi(fileContent){
  //if fileContent is diferent from 'file OK' rejects promise 
  if(fileContent == 'file OK'){
    return new Promise(function(resolve) {
        setTimeout( () => {
          resolve('Response from API')
        }, 3000)
    })
  }else{
    const reason = 'File is not OK';
    return Promise.reject(reason);
  }
}

//prints the message
function sendResponse(apiRes){
  console.log('Sending response: ' + apiRes);
  return;
}

readFile('randmFile.txt').then( responseReadFile => {
  //if readFile() promise is resolved, pass the response to callApi() 
  console.log('readFile() resolved OK. It returned: ' + responseReadFile);
  return callApi(responseReadFile);
}).then( responseCallApi => {
  //if callApi() promise is resolved, pass the respons to sendResponse()
  console.log('callApi() resolved OK. It returned: ' + responseCallApi); 
  return sendResponse(responseCallApi);
}).catch(error => {
  console.log(error.message);
})

