
function calcNumbers(a, b){
  const sum = a + b;
  if(sum > 5){
    return Promise.resolve(sum);
  }else{
    //const err = new Error('REJECTED! Sum is less than 5, it is: ' + sum);
    return Promise.reject(sum);
  }
}

console.log('before first calcNumbers() call');
calcNumbers(3,3).then( res => {
  console.log('RESOLVED! The res was more than 5, it was: ' + res);
}).catch( err => {
  console.log('REJECTED! Sum is less than 5, it is: ' + err);
})
console.log('between calcNumbers() calls');

calcNumbers(1,2).then( res => {
  console.log('RESOLVED! The res was more than 5, it was: ' + res);
}).catch( err => {
  console.log('REJECTED! Sum is less than 5, it is: ' + err);
})
console.log('after both calcNumbers() calls');
