//boolean to decide if promise is resolved or rejected
let conditionToResolve = true;


//create a new Promise using the constructor
let promIsOk = new Promise((resolve, reject) => {     //ES6 syntax arrow func
  if(conditionToResolve){
    const message = "condition was true so promise is resolved";
    console.log(message);
    //the resolve method return the object we pass it
    resolve(message);
  }else{
    const err = new Error("condition was false so promise is rejected");
    console.log(err.message);
    reject(err)
  }
});

//call to the promise
function askProm(){
  promIsOk.then( (resultOfPromise) => {             //ES6 syntax arrow func
    console.log(resultOfPromise);
    conditionToResolve = false;

  }).catch((error) => {
    //catch errors
    conditionToResolve = true;
    console.log(error.message)
   })
}


//this doesnt change the result of the Promise
for (let i = 0; i < 100; i++){
  askProm();
  //change the condition so the next execution returns a different result
  conditionToResolve = !conditionToResolve;
}