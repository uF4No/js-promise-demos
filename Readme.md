# General
This repo contains a few code examples to use JavaScript Promises.

There is an article in my website with more info about it: www.uf4no.com

## How to run
Just execute any of the following commands:
```
npm run prom
npm run nums
npm run actions
npm run actions7
```

## Scripts
  - promiser.js - contains an example of how to use the Promise constructor and a function that uses the created Promise.
  - numericPromise.js - contains a function that returns a Promise and how to call it.
  - actions.js - contains a few async functions and how to call them in chain.
  - actionsES7.js - same as above but using asiync/await syntax.

